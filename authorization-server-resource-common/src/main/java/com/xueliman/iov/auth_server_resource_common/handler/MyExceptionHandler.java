package com.xueliman.iov.auth_server_resource_common.handler;

import com.xueliman.iov.auth_server_resource_common.other.ApiException;
import com.xueliman.iov.auth_server_resource_common.other.SingleResultBundle;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.util.NestedServletException;

/**
 * @author zxg
 * 自定义全局异常拦截
 */
@RestControllerAdvice
public class MyExceptionHandler {
    /**MethodArgumentTypeMismatchException
     * 处理自定义异常
     */
    @ExceptionHandler(ApiException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public SingleResultBundle<String> handleApiException(ApiException e){
        String message = e.getMessage();
        return SingleResultBundle.failed(message);
    }

    @ExceptionHandler(NestedServletException.class)
    public SingleResultBundle<String> handleNestedServletException(NestedServletException e){
        return SingleResultBundle.failed(e.getCause().getCause().getMessage());
    }

    @ExceptionHandler(Exception.class)
    public SingleResultBundle<String> handleException(Exception e){
        return SingleResultBundle.failed(e.getMessage());
    }
}
